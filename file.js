// import internal module for file CRUD Create Read Update Delete)
const fs = require('fs');


const { setUncaughtExceptionCaptureCallback } = require('process');
const fileOperation = require('./abc.js');
console.log('file opreation >>', fileOperation)

write
fs.writeFile('./files/sandesh.txt', 'welcome back to nodejs', {}, function (err, done) {
    if (err) {
        console.log('file wwriting failed', err);
    }
    else {
        console.log('file wwriting success', done)
    }
})


// read
fs.readFile('./files/broadway.txt', 'UTF-8', function (err, done) {
    if (err) {
        console.log('error reading >>', err)
    } else {
        console.log('success in reading >>', done)  // USE 'utf-8' or done.toString()  to convert buffer to string
    }
})

// task ==>
// make it functional
// try using promise as well as callback
// seperate task and execution and run execution file


// rename
fs.rename('./files/broadway.txt', './files/node.js', function (err, done) {
    if (err) {
        console.log('error in renaming>>', err)
    } else {
        console.log('success in renaming', done)
    }
})

// task ==>
// make it functional
// try using promise as well as callback
// seerate task and execution and run execution file

remove
fs.unlink('./files/sandesh.txt', function (err, done) {
    if (err) {
        console.log('error in removing', err)
    } else {
        console.log('success in removing', done)
    }
})

// task ==>
// make it functional
// try using promise as well as callback
// seerate task and execution and run execution file



// make it functional
// callback
// function myWrite(filename,content,cb){
//     fs.writeFile('./files/' + filename,content,function (err,done){
//         if(err){
//             // 
//             cb(err)
//         }else{
//             // 
//             cb(null,done)
//         }
//     })
// }

// promise
function myWrite(filename, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + filename, content, function (err, done) {
            if (err) {
                // 
                reject(err)
            } else {
                // 
                resolve(done)
            }
        })

    })

}

// myWrite('abck.txt','test something')
// .then(function(data){
//     console.log('success....',data)
// })
// .catch(function(err){
//     console.log('error in writing',err)
// })

// myWrite('broadway.txt','hello',function(err,done){
//     if(err){
//         console.log('error in writing',err)
//     }else{
//         console.log('success .. ')
//     }
// })


// fileOperation.a('node.js','lets learn nodejs',function (err,done){
//     if (err){
//         console.log('error in wwrite',err)
//     }
//     console.log('success in write',done)
// })