module.exports = function (req, res, next) {
    if (req.query.ticket === 'something') {
        next();
    }
    else {
        next({
            msg: 'invalid ticket',
            status: 400
        })
    }
}