const multer = require('multer');
const path = require('path');
// quick usage
// const uploader = multer({
//     dest : path.join(process.cwd(),'uploads')
// })

module.exports = function (filterType) {

    function imageFilter(req, file, cb) {
        // file filter will skip the upload
        var mimeType = file.mimetype.split('/')
        // mimetype == ['image','png']
        console.log('mime type is >>', mimeType);
        if (mimeType[0] === 'image') {
            cb(null, true) //proceed with upload
        } else {
            req.fileTypeError = true;
            cb(null, false)// skip upload
        }
    }


    const myStorage = multer.diskStorage({
        filename: function (req, file, cb) {
            cb(null, Date.now() + '-' + file.originalname)

        },
        destination: function (req, file, cb) {
            cb(null, path.join(process.cwd(), 'uploads/images'))

        }
    })
    //TODO use filterType to filter the upload

    const upload = multer({
        storage: myStorage,
        fileFilter: imageFilter

    })

    return upload

}




