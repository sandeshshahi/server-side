const router = require('express').Router();

// impport middlewares
const authorize = require('./middlewares/authorize');




// import routing level middleware
const AuthRouter = require('./controllers/auth.controller');
const UserRouter = require('./controllers/users.controllers');
const NotificationController = require('./controllers/notification.controller');
const ProductRouter = require('./components/products/product.route');

router.use('/auth', AuthRouter)
router.use('/user', authorize, UserRouter)
router.use('/notifications', authorize, NotificationController)
router.use('/product', ProductRouter)
router.use('/message', UserRouter)


module.exports = router;