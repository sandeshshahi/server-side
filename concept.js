// NODE ===> JS run time environment
// server side JS
// database communication

// command
// node --version or -v
// npm --version
// npx --version

// npm and npx are package management tool
// npm
// yarn similar to npm

// npx TODO discuss when starting react

// npm init
// ==> initialize JS project (regardless of application area)

// it will help to create package.json
// package.json file holds overall information about project
// try to keep updated information inside package.json

// package-lock.json ===> this file will lock the version of dependent packages
// installation version lock


// npmjs.com npm registry ==> container that holds javascript projects
// npm install <package.name>
// populate package.json file >> package-lock.json, node_modules folder
// node_modules folder wwhere installed packages are kept

// npm install ===> checks dependency section of package.json an install all the dependency



// to import all the modules from node_modules folder and nodejs internal moule
// dont give path
// eg const events = require('events')

// give path for your own files
// const a = require('./myFile')


// Web Architecture (3 tier architecture bhitra parcha)
// MVC 
// separation of concerns
// Models
// Views
// Controller

// tier architecture 
// 3 tier architecture (MVC)
// presentation layer (Views)
// application layer (Controller)
// data layer (models)

// Client-Server Architecture
// ===> communication between two programme
// protocol must be same for communication
// request pathaune programme is Client
// request accept garne ,process garne and response  programme is server


// research topic
// architecture vs design pattern



// HTTP protocol(Hyper Text Trasfer Protocol)
// HTTP verb(methods)
// GET,POST,PUT,DELETE,PATCH
// http url

// request object
// response object


// http status code
// 100, ===> informational response
// 200, ===> success
// 300, ===> redirection
// 400, ===> application ko error or client error
// 500, ===> server ko error


// REST API

// API ==> Application Programming Interface
// endpoint ==> combination of methods and URL
// eg /api/auth/login POST. eg, /product GET
// 


// SOAP
// ##### REST Architecture ==> Representational State Transfer
// GraphQL

// To be on REST Architecture the following points neeeds to be fulfilled
// 1. Stateless
    // server le client ko request save garnu vayena
    // stateless protocol-->> each and every request is independent

// 2. Correct Use Of HTTP Verb
// GET ==> Data Fetch
// POST ==> Data Send ===> Insert
// PUT/PATCH ==> Update
// DELETE ==> Remove

// 3. Data Format must be either in JSON or in XML
// JSON is used everywhere

// 4. GET method's Request can be cached


