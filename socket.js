const socket = require('socket.io');
const config = require('./configs');


module.exports = function (app) {
    const io = socket(app.listen(config.SOCKET_PORT), {
        cors: {
            origin: '*'
        }
    })
    io.on('connection', function (client) {
        console.log('socket client connected to server', client.id)

        client.emit('hi', 'welcome to server')

        client.on('hello', function (data) {
            console.log('at hello >>', data)
        })
    })


}