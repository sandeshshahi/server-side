// node js internal module ---http

const http = require('http');
const fileop = require('./abc');


// browser ko url hit garda jahile pani GET request jancha
const server = http.createServer(function (request, response) {
    console.log('http client connected to server')
    console.log('http method >>', request.method);
    console.log('http url>>', request.url);

    if (request.url === '/write') {
        // write operation
        fileop
            .b('broadway.txt', 'broadway infosys nepal')
            .then(function (data) {
                response.end('file writing success', data)
            })
            .catch(function (err) {
                response.end('file writing failed', err)
            })
    }
    else if (request.url === '/read') {
        // read operation
    }
    else {
        response.end('noo any action to perform')
    }

    // request response cycle must be completed
    // response.end('hello from node server')
    // request or 1st arg placceholder is http protocol ko request object
    // response or 2nd arg placeholder is http response object

    // callback
    // this callback will be executed once client sends requests


    // for http communication
    // url and method must be there
    // endpoint ==> combination of http method and url
    // regardless of http method and regardless of http url this callback will be executed


})
// server is programme


// programme vs process
// programme is a set of instruction


server.listen(9000, '127.0.0.1', function (err, done) {
    if (err) {
        console.log('server listening fail', err)
    } else {
        console.log('server listening at port 9000 inside', done)
        console.log('press Ctrl+c to cancel')
    }
})

