// Import controller
const productCtrl = require('./product.controller');
const router = require('express').Router();
const authorize = require('./../../middlewares/authorize');
const Uploader = require('./../../middlewares/uploader')('image');

router.route('/')
    .get(authorize, productCtrl.get)
    .post(authorize, Uploader.array('images'), productCtrl.post)

router.route('/search')
    .get(productCtrl.search)
    .post(productCtrl.search)
router.route('/add_review/:product_id')
    .post(authorize, productCtrl.addReview);

router.route('/:id')
    .get(authorize, productCtrl.getById)
    .put(authorize, Uploader.array('images'), productCtrl.update)
    .delete(authorize, productCtrl.remove);

module.exports = router;