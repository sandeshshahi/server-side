// database ==> container that holds data
// harddrive partition jasma programatically database ko operation garincha
// database management system DBMS

// DBMS types
// 1. Relational Database Management System
// 2. Distributed DBMS (Non Relation DBMS)

// 1. Relational Database Management System
// a. table based design
// eg LMS(library management system) ==> entities ===> Books,Users,Reviews,Notifications,
// table ==> storage of records
// b. each record are called tuple /  row
// c. schema design ==> schema validation of properties to be stored in table
// d. relation between table exists
// e. SQL database (Structured Query Language)
// f. types mysql,sql-lite,postgres,mssql, ....
// g. non scalable 


// 2. Distributed Database Management System (non-relational)
// a. collection based design
// eg LMS ==> entities ==> Books, USers
// books,users are collections
// b. each record in a collections are called document
// document based database
// a document can be valid javascript object
// c. schema less design 
// no any validation and restriction
// d. highly scalable
// e. relation doesnt exists
// f. NoSQL ==> Not Only SQL
// g. mongodb,couchdb,dynamodb,redis(in memory database)

// eg of books
/*
var books1 = {
    name:'prayogsala',
    genre:'politics',
    reviews:[
        {
            point:2,
        message:'good'

        },
        {
            point:2,
        message:'good'

        },
        {
            point:2,
        message:'good'

        },

    ],
    author:{
        name:'sandesh',
        dob:'333',
        username:'sndshshahi'
    }
}

*/

// Why MONGODB?

// CLIENT SIDE (front end)==> react[JS]
// SERVVER SIDE (back end)===> Node/express [JS]
// DATABASE ==> JSON ==> [JS]


// MONGODB
// client server communication

// mongodb application(server)
// mongod ==> driver initialization
// 2701
// mongodb server
// mongodb://localhost:27017


// http server
// url ---> http://localhost:8080

// client ko programme
// 1. shell as an client
// 2. UI TOOL
// 3. Mongod db official driver for nodejs
// 4.ODM tool



// 1. mongoshell
//  for connection type mongo command at any location
// once we have > interface our terminal is ready to accept shell command
// shell command
// 1. show dbs ==> list all the available databases
// 2. to create database
// command ==> use <db_name>
// if (existing db )select existing database else create new database and select it
// db ==> shows selected database
// show collections ==> list all the available collections of selected database

// ddb operation
// CRUD OPERATION
// to add collections (C create)
// db.<collection_name>.insert({valid json}) either array or object
// db.<collection_name>.insertMany(array)

// R Read
// db.<collection_name>.find({query_builder})
// db.<collection_name>.find({query_builder}).pretty();// formatted outputs
// db.<collection_name>.find({query_builder}).count();// returns document counts
// db.<collection_name>.find({query_builder}).sort({_id:-1});// dedscending order (timestamp)
// db.<collection_name>.find({query_builder}).limit(number);// limit result
// db.<collection_name>.find({query_builder}).skip(number);//skip document

// projection ===> inclusion or exclusion
// eg, db.<collection_name>.find({},{key:1[for_inclusion],key:0[for exclusion]})
// price range $gt,$gte,$lt,$lte

// U update
// db.<collection_name>.update({},{},{});
// 1st object ==> query buildder
// 2nd object ==> will have $set as key and value as another object
// another object is payload to be updated
// 3rd object ==> options multi,upsert

// eg
// db.<collection_name>.update({_id:ObjectID('id)},{$set:{pricessor:'i9'}},{multi:true,upsert:true})

// D delete
// db.<collection_name>.remove({condition})
// NOTE dont leave condition empty

// drop collection
// db.<collection_name>.drop();

// drop database
// db.dropDatabase();

// ODM || ORM
// ODM ==>Object Document MOdelling (Ocument basedd database)
// ORM ==>Object Relation Mapping (SQL database)

// ODM tool for nodejs and mongodb
// mongoose

// advantages of using mongoose
// 1. Schema based solution
//      ->properties with their data types in a document

// 2. Indexing are lot more easier 
        // -> unique,required,

// 3. Data type restrictions
        // ->number,boolean,date

// 4. Middleware
        // -> pre hooks, 

// 5. methods
        // -> find,update,emove,inserOne,insertMany
        // -> other than this we have several methods provided by mongoose


// now db connection will be open once server is up 
// focus on query only



// BACKUP AND RESTORE
// command
// mongodump and mongorestore
// mongoexport and mongoimport


// two way of back and restore
// bson
// human readable JSON and CSV

//bson format
    // backup
    // command --> mongodump
    // mongodump ==> it will create backup of all the available database under ddefault dump folder
    // mongodump --db <selected_db_name> ==> it will backup only selected  db under dump folder


    // restore
    // command --> mongorestore
    // mongorestore ==> it will look after dump folder and tries to backup database
    // mongorestore --drop ==> it will look after dump folder drop existing document and restore or in other word existing data will be replaced with data that was in backup
    // mongorestore <path_to_source_folder> ==> checks for dumpfile/bson backup with given folder location



// JSON and CSV format

// 1. JSON
    // backup
    // command ==>  mongoexport
    // mongoexport --db <bd_name> --collection <collection_name> --out <path_to_destination_folder_with_.json_Extension>
    // mongoexport --d <bd_name> --c <collection_name> --o <path_to_destination_folder_with_.json_Extension>


    // restore
    // command ==> mongoimport
    // mongoimport --db <new or existing_db> --collection <new or existing_collection> <path_to_source_file_with_.json_extension>


// 2. CSV
    // backup
    // command --> mongoexport
    // mongoexport --db <db_name> --collection <collection_name> --type=csv --fields 'comma,separatedd property_name' --out <path_to_destination_with_.csv_extension>
    // with query ==> mongoexport --db <db_name> --collection <collection_name> --query='{"<property>":"<value>"}'--type=csv --fields 'comma,separated property_name' --out <path_to_destination_with_.csv_extension>


    // import/ restore
    // command --> mongoimport
    // mongoimport --db<dbname> --collection<collection_name> --type=csv <path_to_csv_file> --headerline
