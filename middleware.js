const express = require('express');
const PORT = 8080;
const morgan = require('morgan');

const app = express();
// app is now entire express framework

// load third party midleware
app.use(morgan('dev'))

// endpoint specific handler
// endpoint ==> combination of method and url


// app.use() configuration block for middleware
// function(req,res,next){}//middleware function

/* yesari pani lekhna milyo middleware
function checkticket(req,res,next){

}
function validateTicket(req,res,next){

}

app.use(checkticket,validateTicket)

*/

// regardless of any http method and http url
// this middleware will come into action
app.use(function (req, res, next) {
    console.log('application middleware')
    next();
})

// regardless of method but url must be /home to execute this middleware
app.use('/home', function (req, res, next) {
    // application level middleware
})

// method and url specific
app.get('/help', function (req, res, next) {

})

// endpoint specific
app.post('/payment', function (req, res, next) {

})


app.get('/', function (req, res) {
    // handler for empty url and get method
    // req or 1st arg placeholer is http request object
    // res or 2nd arg placeholer is http response object
    // request response cycle must be completed
    // response complete garnalai res (2nd arg) use garnu paryo
    // res.send() //anything can be sent
    // res.json();
    // res.sendStatus();
    // res.sendfile();
    // res.download();
    // res.sendStatus(200)
    res.json({
        text: 'hello from empty url'
    })

    // res.status() //it will not complete req-res cycle
    // it only sets status code in response
    // response must be sent only once

    // dev-work
    // accept
    // parse
    // validate
    // db operation
    // response complete
})
app.get('/login', function (req, res) {
    res.send('from login get')
    // handler for post method and /login url
})


app.get('/write/:filename', function (req, res) {
    //handler for post method and /login url
    // write work
    // promise or callback
    // response of write should be sent to client
    // values from part of url can be extracted from req object
    // params will hold the part of url values

    res.json({
        msg: 'from write',
        params: req.params

    })
})

app.get('/register', function (req, res) {

})



app.listen(PORT, function (err, done) {
    if (err) {
        console.log('server listening fail')
    }
    else {
        console.log('server listening at port', PORT)
    }
})


// middleware
// middleware is a function that has access to
// http request object
// http response object
// and next middleware function reference
// syntax
// function(req,res,next){
    // req or 1st argument is always http request object
    // res or 2nd argument is always http response object
    // next or 3rd argument is always next middleware function reference
// }

// it is very powerful function which can access, modify http request response
// middleware's object is very very important
// middleware function came into action between req-res cycle


//middleware configuration
// use method, HTTP VERB, all

// types of middleware
// 1. application level middleware
// 2. routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. error handling middleware

// application level middleware
// middleware having scope of http request object , http response object and next reference are application level middleware

// above examples of middleware are application level middleware
// request's handler are always application level middleware




// app.use(function(req,res,next){
//     console.log('i am middleware function');
//     console.log('i will present in every http req-res cycle');
//     // res.json({
//     //     msg: "blocked from middleware"
//     // })
//     req.broadway = 'infosys nepal'
//     next();// arko middleware call garnu ho
//     // control pass gariyo
// },function (req,res,next){
//     next();
// })

// app.use(function(req,res,next){
//     console.log('i am 2nd middleware');
//    next();
// })