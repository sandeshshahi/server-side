const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader')();
const NotificationModel = require('./../models/notification.model');
const removeFile = require('./../helpers/remove_file');

// sub route
router.route('/')
    .get(function (req, res, next) {
        // get all user
        // projection (2nd argument of find){
        //username:1,
        //password:1,
        //_id:0})

        UserModel
            .find({})  //find({},{
            //username:1,
            //password:1,
            //_id:0})
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users)
            })
    })
    .post(function (req, res, next) {
        // add user
    });

// router.route('/search')
//     .get(function(req, res, next){
//             res.json({
//                 msg:'from user search'
//             })
//     })
//     .post(function(req, res, next){
//             res.send('from post of search')
//     })
//     .put(function(req, res, next){

//     })
//     .delete(function(req, res, next){

//     });

router.route('/:userID')
    .get(function (req, res, next) {
        let id = req.params.userID;
        // find by id
        UserModel.findById(id)
            .then(function (user) {
                if (!user) {
                    return next({
                        msg: "user not found",
                        status: 404
                    })
                }
                res.json(user)
            })
            .catch(function (err) {
                next(err);
            })

    })

    .put(Uploader.single('image'), function (req, res, next) {
        console.log('req.body>>', req.body);
        console.log('req.file>>', req.file)
        if (req.file) {
            req.body.image = req.file.filename;
        }

        // UserModel.findByIdAndUpdate(id,update:{})
        UserModel.findById(req.params.userID, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "user not found",
                    status: 404

                })
            }
            var oldImage = user.image; //most important to know yeslai map user update bhaye pacchi rakhyo bhane due to mutable behaviour upload gardai gareko image nai delete  huncha so data base ma image ko name bascha tara server ma image hudaina
            // if user exist proceed with update
            // user is also mongoose object


            const updatedMapUser = MAP_USER_REQ(user, req.body);

            // once user object is update with provided with updated value

            updatedMapUser.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                // TODO remove existing image of user
                res.json(updated)
                // server cleanup
                if (req.file) {
                    removeFile(oldImage);
                }
                const newNotification = new notificationModel({});
                newNotification.message = 'your profile is updated';
                newNotification.user_id = user._id;
                newNotification.category = 'general';
                newNotification.save();
                // no result handling for now

            })
        })
    })


    .delete(function (req, res, next) {
        // UserModel.findByIdAndRemove(req.params.user_id,callback)
        UserModel.findById(req.params.userID, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "user not found",
                    status: 404
                })

            }
            user.remove(function (err, removed) {
                if (err) {
                    return next(err);
                }
                res.json(removed)
                removeFile(user.image);
            })

        })

    });

module.exports = router;