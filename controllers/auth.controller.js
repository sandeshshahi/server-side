const { request } = require('express');
const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MAP_USER_REQ = require('./../helpers/map_user_req');
const Uploader = require('./../middlewares/uploader')('image');
const passwordhash = require('password-hash');
const JwT = require('jsonwebtoken');
const config = require('./../configs/index');
const randomStr = require('randomstring');
const sender = require('./../configs/email.config');
const myEv = require('./../app');



function prepareEmail(data) {
    return {
        from: '"cushy buy" <foo@example.com>', // sender address
        to: "sandesh4366@gmail.com,sndshshahi@gmail.com," + data.email, // list of receivers
        subject: "forgot password", // Subject line
        text: "Hello world?", // plain text body
        html: `<div>
        <p>Hi <strong>${data.name}</strong></p>
        <p>We noticed thaat you are having trouble logging into out system please use link below to reset your password.</p>
        <p><a href="${data.link}">click here to reset password</a></p>
        <p>Regards</p>
        <p>cusy buy</p>
        <p>${new Date().getFullYear()}</p>

        </div>`, //html body
    }
}


function generateToken(data) {
    let token = JwT.sign({
        _id: data._id,
    }, config.JWT_SECRET);
    return token;

}

router.get('/', function (req, res, next) {
    require('fs').readFile('sadasdasdas.asdsad', function (err, done) {
        if (err) {
            req.myEv.emit('error', err)
        }
    })

})

// const mongodb = require('mongodb');
// const MONGO_CLIENT = mongodb.MongoClient;
// const CONXN_URL = 'mongodb://localhost:27017';
// const DB_NAME = 'merndb';


// const router = express.Router();
// // router is routing level middleware


// router.get('/',function(req,res,next){
//     res.render('index.pug',{
//         title:'nodejs',
//         message:'lets learn mongodb from tomorrow'
//     })
// })

// router.get('/login',function(req,res,next){
//     res.render('login.pug');
// })

router.post('/login', function (req, res, next) {
    console.log('at post request of login', req.body);
    // views ra controller communication success
    // data is already in controller
    // db_stuff
    // res.json({
    //     msg:"at login",
    //     method:"POST",
    //     data:req.body
    // })
    // MONGO_CLIENT.connect(CONXN_URL,{
    //     useUnifiedTopology:true
    // },function(err,client){
    //     if (err){
    //         return next(err);
    //     }
    //     const db = client.db(DB_NAME);
    //     db
    //     .collection('users')
    //     .find({username:req.body.username})
    //     .toArray(function(err,users){
    //         if (err){
    //             return next(err);
    //         }
    //         res.json(users)  
    //     })
    // })

    UserModel
        .findOne({
            $or: [
                {
                    username: req.body.username
                }, {
                    email: req.body.username
                }
            ]

        })
        .then(function (user) {
            if (!user) {
                return next({
                    msg: "invalid username",
                    status: 404

                })
            }
            // password verification
            const isMatched = passwordhash.verify(req.body.password, user.password);
            if (!isMatched) {
                return next({
                    msg: 'Invalid Password',
                    status: 400
                })
            }

            // is active user
            // res.json(user);
            if (user.status === 'inactive') {
                return next({
                    msg: "your account is disabled please contact system administrator for support",
                    status: 403
                })
            }
            // token generation
            var token = generateToken(user);

            res.json({
                user: user,
                token: token
            })

        })
        .catch(function (err) {
            next(err);
        })


})


router.post('/register', Uploader.single('image'), function (req, res, next) {
    // 
    console.log('req.body>>', req.body)
    console.log('req.file>>', req.file)
    if (req.fileTypeError) {
        return next({
            msg: 'invalid file format',
            status: 405
        })
    }

    const data = req.body;

    if (req.file) {
        data.image = req.file.filename;
    }
    // db operation
    const newUser = new UserModel({});
    //newUser is mongoose object

    // default values with _id and --v will be available for every instance with methods

    // newUser.firstName = req.body.first_name;
    // newUser.lastName = req.body.last_name;
    // newUser.email=req.body.email;
    // newUser.contactDetails = {
    //     mobile:req.body.mobileNumber,
    //     home:req.body.homeNumber,
    //     alternateNumber:req.body.alternateNumber
    // }
    // newUser.dob = req.body.date_of_birth;
    // newUser.gender= req.body.gender;
    // newUser.address = {
    //     temporaryAddress:req.body.tempAddress ? req.body.tempAddress.splot(','):[],
    //     permanentAddress:req.body.permanentAddress
    // }

    // newUser.username = req.body.username;
    // newUser.password=req.body.password;

    const newMapperUser = MAP_USER_REQ(newUser, data)

    newMapperUser.password = passwordhash.generate(req.body.password)


    // mongoose method
    newMapperUser.save(function (err, saved) {
        if (err) {

            return next(err);
        }
        res.json(saved)
    })



    // db connection
    // MONGO_CLIENT.connect(CONXN_URL,function(err,client){
    //     if(err){
    //         console.log("db connection failed");
    //         return next(err);
    //     }
    //     console.log('db connection successfull')
    //     // db operation
    //     // select db
    //     const selected_db = client.db(DB_NAME);
    //     // query
    //     selected_db
    //         .collection('users')
    //         .insertOne(data)
    //         .then(function(result){
    //             res.json(result)

    //         })
    //         .catch(function(err){
    //             next(err)

    //         })
    // })
})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            next({
                msg: "email not registered yet",
                status: 404
            })
        }
        // user found now proceed with email
        const passwordResetToken = randomStr.generate(29);
        const passwordResetExpiry = Date.now() + 1000 * 60 * 60 * 24;
        const emailBody = {
            name: user.username,
            email: user.email,
            link: `${req.headers.origin}/reset_password/${passwordResetToken}`
        }
        const emailContent = prepareEmail(emailBody);
        user.passwordResetToken = passwordResetToken;
        user.passwordResetExpiry = passwordResetExpiry;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            sender.sendMail(emailContent, function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
    })
})



router.post('/reset-password/:token', function (req, res, next) {
    const token = req.params.token;
    UserModel.findOne({
        passwordResetToken: token,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "Invalid/Expired Password Reset Token",
                status: 400
            })
        }
        // check if it is not expired
        // if (Date.now() > user.passwordResetExpiry) {
        //     return next({
        //         msg: 'Password Reset Link Expired'
        //     })

        // }


        user.password = passwordHash.generate(req.body.password)
        user.passwordResetToken = null;
        user.passwordResetExpiry = null;
        user.save(function (err, saved) {
            if (err) {
                return next(err);
            }
            res.json(saved);
        })
    })
})

module.exports = router;