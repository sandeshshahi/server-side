const notificationModel = require('./../models/notification.model');
const NotificationModel = require('./../models/notification.model');
const router = require('express').Router();

router.get('/', function (req, res, next) {
    // get all notification
    var condition = {
        user: req.user._id
    };
    notificationModel
        .find(condition)
        .sort({
            _id: -1
        })
        .limit(10)
        .populate('user_id'), {
            username: 1
        }
            .exec(function (err, result) {
                if (err) {
                    return next(err);
                }
                res.json(result);
            })
})

router.get('/:id', function (req, res, next) {
    notificationModel.findById(req.params.id, function (err, noti) {
        if (err) {
            return next(err)
        }
        res.json(noti);
    })
})

router.get('/mark_as_read/:id', function (req, res, next) {
    // 
    notificationModel.update({ _id: req.params.id }, {
        $set: { seen: true }
    }, {
        multi: true
    }, function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done)
    })

})

router.get('/mark_all_as_read', function (req, res, next) {
    // 
    notificationModel.update({
        user: req.user._id
    }, {
        $set: { seen: true }
    }, {
        multi: true
    }, function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done)
    })
})


router.delete('/:id', function (req, res, next) {
    // 
    notificationModel.findByIdAndRemove(req.params.id, function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done)
    })

})

module.exports = router;