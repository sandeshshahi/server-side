const express = require('express');
const morgan = require('morgan');
const path = require('path');
const configs = require('./configs');
const { PORT } = require('./configs');
const cors = require('cors');

const app = express();
// app is now entire express framework

require('./db_initialize.js'); // part of app.js file will run above file

// part of app.js call socket file with sending some value
require('./socket')(app)

// event stuff
const events = require('events');
const myEv = new events.EventEmitter();
app.use(function (req, res, next) {
    req.myEv = myEv;
    next();
})

myEv.on('error', function (err, res) {
    console.log('error event of own >>', err)
    res.json(err);
})


const api_route = require('./api.route');

// load third party midleware
app.use(morgan('dev'));
app.use(cors()); //accept every request

// keywords globals
// __dirname 
// console.log('__dirname >>',__dirname)
// console.log('root directory path >>', process.cwd())

// inbuilt middleware for serving static files
// app.use(express.static('uploads')) //internal serve (express as an inddependent application)
app.use('/files', express.static(path.join(__dirname, 'uploads'))) //internal as well as externals

// aall incoming data must be parsed according to their content type
//xml,form-urlencoded,application/json,textual,html
// x-www-form-urlencoded parser
app.use(express.urlencoded({
    extended: true
}))
// this middleware will add body property with value as an object in http request object

// json parser
app.use(express.json());







// mount incoming request
// authentication related endpoint
app.use('/api', api_route)


// 404 catch middleware
app.use(function (req, res, next) {
    // application level middleware
    next({
        msg: 'not found',
        status: 404
    })
})

// middleware having 4 arguments is error handling middleware
// syntax
// function(a,b,c,d){
//     // a or 1st argument is for error
//     // b,c,d are request,response,next
// }
// error handling middleware never come in between request response cycle
// error handling middleware must be called
// next with argument will execute error handling middleware

app.use(function (error, req, res, next) {
    console.log('at error handling middleware is >>', error)
    //    all the errors of the application must be sent to error handling middleware
    //  TODO set status code in the response to identify success or failure
    res.status(error.status || 400)
    res.json({
        msg: error.msg || error,
        status: error.status || 400
    })
})


app.listen(process.env.PORT || configs.PORT, function (err, done) {
    if (err) {
        console.log('server listening fail')
    }
    else {
        console.log('server listening at port', PORT)
    }
})


// middleware
// middleware is a function that has access to
// http request object
// http response object
// and next middleware function reference
// syntax
// function(req,res,next){
    // req or 1st argument is always http request object
    // res or 2nd argument is always http response object
    // next or 3rd argument is always next middleware function reference
// }

// it is very powerful function which can access, modify http request response
// middleware's object is very very important
// middleware function came into action between req-res cycle


//middleware configuration
// use method, HTTP VERB, all

// types of middleware
// 1. application level middleware
// 2. routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. error handling middleware

// application level middleware
// middleware having scope of http request object , http response object and next reference are application level middleware

// above examples of middleware are application level middleware
// request's handler are always application level middleware




// app.use(function(req,res,next){
//     console.log('i am middleware function');
//     console.log('i will present in every http req-res cycle');
//     // res.json({
//     //     msg: "blocked from middleware"
//     // })
//     req.broadway = 'infosys nepal'
//     next();// arko middleware call garnu ho
//     // control pass gariyo
// },function (req,res,next){
//     next();
// })

// app.use(function(req,res,next){
//     console.log('i am 2nd middleware');
//    next();
// })